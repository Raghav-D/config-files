#include <iostream>
#include <fstream>


int main(int argc, char *argv[]) {

    std::fstream file;

    file.open("file", std::ios::app);
    if (!file)
    {
        std::cout << "The database file was not successfully opened. Please try again." << std::endl;
    }
    else
    {
        short int action;

        std::cout << "The database file was opened successfully.\nWhat action do you want to perform?" << std::endl;
        std::cout << "1. Add Student\n2. Delete Student\n3. Modify Student\n\n4. Add Teacher\n5. Delete Teacher\n6. Modify Teacher\n\n\n=>";

        std::cin >> action;

        std::cout << action;
    }

    return 0;
}


// #include <fstream>
// #include <iostream>

// {
//     // ofstream is used for writing files
//     // We'll make a file called Sample.dat
//     std::ofstream file { "Sample.txt" };

//     // If we couldn't open the output file stream for writing
//     if (!file)
//     {
//         // Print an error and exit
//         std::cout << "Uh oh, Sample.txt could not be opened for writing!" << std::endl;
//         return 1;
//     }

//     // We'll write two lines into this file
//     file << "This is line 1" << '\n';
//     file << "This is line 2" << '\n';

//     return 0;

//     // When outf goes out of scope, the ofstream
//     // destructor will close the file
// }
