function emacs --wraps='open -a Emacs' --description 'alias emacs open -a Emacs'
  open -a Emacs $argv; 
end
