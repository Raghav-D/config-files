function scala3 --wraps='java -cp (cs fetch -p org.scala-lang:scala3-library_3:3.0.0):. ' --description 'alias scala3 java -cp (cs fetch -p org.scala-lang:scala3-library_3:3.0.0):. '
  java -cp (cs fetch -p org.scala-lang:scala3-library_3:3.0.0):.  $argv; 
end
