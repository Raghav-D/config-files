set EDITOR "emacsclient -t -a ''"
set VISUAL "emacsclient -c -a emacs"
set TERM   "xterm-256color"


function fish_greeting
    /opt/local/bin/clear # Clear the screen when fish starts
end

### ALIASES
alias cd6 "cd ~/OneDrive/Documents/MatsumotoSchool/SixthGrade"
alias la "ls -A"
alias gs "git status"
alias mv "mv -i"
alias rm "rm -i"
alias nv "nvim"
alias bwin "brew install "

alias bwca "brew install --cask "
alias cp "cp -i"
alias df "df -h"
alias c "clear"
alias st "speedtest"
alias config "nvim ~/.config/fish/config.fish"
alias a "open -a "
alias fullpwd "pwd"
alias agenda "nvim ~/OneDrive/Documents/MatsumotoSchool/SixthGrade/Agenda/April.md"
alias gtop "~/gtop/bin/gtop"
alias ls "exa -alhSU --icons --octal-permissions"
alias lt "exa -lhSUT --icons --octal-permissions"
alias cf '/opt/local/bin/clear'
alias doas "doas --"
alias mkdir "mkdir -p"
alias mkcd "mkdir -p $1; cd $1"
alias cdl "cd $1; ls"

alias rr='curl -s -L https://raw.githubusercontent.com/keroserene/rickrollrc/master/roll.sh | bash'

function fish_user_key_bindings
    fish_vi_key_bindings # Sets vi keybindings
end


function __history_previous_command_arguments
  switch (commandline -t)
  case "!"
    commandline -t ""
    commandline -f history-token-search-backward
  case "*"
    commandline -i '$'
  end
end

function __history_previous_command
  switch (commandline -t)
 case "!"
    commandline -t $history[1]; commandline -f repaint
  case "*"
    commandline -i !
  end
end

bind ! __history_previous_command
bind '$' __history_previous_command_arguments
set PATH "/opt/local/bin:/opt/local/sbin:$PATH"


alias df='df -h'

set -x MANPAGER "sh -c 'col -bx | bat -l man -p'"


set -x PKG_CONFIG_PATH /opt/local/lib/pkgconfig

export MICRO_TRUECOLOR=1

set -e fish_user_paths
set -U fish_user_paths $HOME/.local/bin $HOME/Applications $fish_user_paths

function __history_previous_command
  switch (commandline -t)
  case "!"
    commandline -t $history[1]; commandline -f repaint
  case "*"
    commandline -i !
  end
end

function __history_previous_command_arguments
  switch (commandline -t)
  case "!"
    commandline -t ""
    commandline -f history-token-search-backward
  case "*"
    commandline -i '$'
  end
end

#alias doomsync "~/.emacs.d/bin/doom sync"
#alias doomdoctor "~/.emacs.d/bin/doom doctor"
#alias doomconfig "emacsclient ~/.config/doom/config.org"
#alias doomupgrade "~/.emacs.d/bin/doom upgrade"
#alias doompurge "~/.emacs.d/bin/doom purge"

#zoxide init fish | source
#hexyl init fish | source


set NIX_LINK $HOME/.nix-profile/etc/profile.d/nix.sh
export NIX_PATH="nixpkgs=$HOME/.nix-defexpr/channels/nixos-21.05"
export PATH="$PATH:/Users/RaghavDixit/Library/Application Support/Coursier/bin"
set -gx PATH $PATH "$HOME/doom/bin"
set -gx PATH $PATH "$HOME/.nix-profile/bin/"
alias ed "ed -p ':' "
set -gx PATH $PATH "$HOME/dwm/"
set -gx PATH $PATH "$HOME/.cargo/bin/"

starship init fish | source
set -g fish_prompt_pwd_dir_length 10
fish_add_path /usr/local/opt/llvm/bin
