[colors]
border_color = "#666699"


[[row]]
  ratio=30
  [[row.child]]
  ratio=60
  type="cpu"
  [[row.child]]
  ratio=40
  type="net"
[[row]]
    ratio=40
    [[row.child]]
      ratio=4
      type="mem"
    [[row.child]]
      ratio=3
      [[row.child.child]]
        type="temp"
      [[row.child.child]]
        type="disk"
[[row]]
  ratio=50
  [[row.child]]
    type="proc"
    default=true
