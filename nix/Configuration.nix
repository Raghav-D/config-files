{ config, lib, pkgs, ... }:

{
  imports = [
    ./hardware-configuration.nix
  ];


  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "/dev/sda";

  networking.hostname = "rd-nixos";
  networking.wireless.enable = true;
  networking.useDHCP = false;
  networking.interfaces.enp1s0.useDHCP = true;

  time.timeZone = "Asia/Mumbai";

  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Teriminus16";
    keyMap = "us";
  };

  # Desktop Environments/Window Managers/Display Managers
  services.xserver.enable = true;
  services.xserver.videoDrivers = [ "intel" ];
  services.xserver.displayManager.sddm.enable = true;
  services.xserver.desktopManager.xfce.enable = true;
  services.xserver.displayManager.defaultSession = "none+awesome";
  services.xserver.windowManager = {
    awesome.enable = true;
    i3.enable = true;
    herbstluftwm.enable = true;
    xmonad.enable = true;
    xmonad.enableContribAndExtras = true;
    xmonad.extraPackages = hpkgs: [
      hpkgs.xmonad
      hpkgs.xmonad-contrib
      hpkgs.xmonad-extras
    ];
  };

  qt5.enable = true;
  qt5.platformTheme = "gtk2";
  qt5.style = "gtk2";

  # Keyboard Layout
  services.xserver.layout = "us";
  services.xserver.xkboption = "workman";

  services.printing.enable = true;

  # Audio
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  services.xserver.libinput.enable = true;

  # User
  users.users.rd = {
    isNormalUser = true;
    home = "/home/rd";
    description = "Raghav Dixit";
    extraGroups = [ "wheel" "networkmanager" ];
  };

  environment.systemPackages = with pkgs; [
    # Terminals & Command Line
    kitty
    git
    xarg.xkill
    lxsession
    wget
    htop

    # Text Editing
    emacs
    neovim
    notepadqq

    # GUI Applications
    brave
    pcmanfm
    teams

    # Window Manager Stuff
    polybar
    xmobar
    picom
    nitrogen
    dmenu
  ];
}
