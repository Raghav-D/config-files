#+TITLE: Configuration Files

Here are the dotfiles for the programs I use!

* How I did it
NOTE: I would recommend to have a backup in case anything goes wrong.
1. Move all dotfiles to a folder called =~/.dotfiles= or =~/.files=. The name can be anything, but these have nice recognition.
  #+BEGIN_SRC sh
mkdir ~/.dotfiles
mv ~/.config/ ~/.dotfiles/
mv ~/.emacs.d/ ~/.dotfiles/
# Do this for all your dotfiles
  #+END_SRC
Try and keep the same file structure as in your home directory, or stow will link files to the wrong places. On some distributions of Linux, you might have distro-specific folders in your ~~/.config~ folder. Do not add these to ~~/.dotfiles~, ir it may not be reproducable on other distros.
2. Install GNU Stow
   #+begin_src sh
# Nix (NixOS):
nix-env -iA nixos.stow

# Nix (non-NixOS)
nix-env -iA nixpkgs.stow

# Arch Based
sudo pacman -S stow

# Redhat Based
sudo dnf install stow

# Debian Based
sudo apt install stow

# macOS (Port)
sudo port selfupdate
sudo port install stow

# macOS (Brew)
brew install stow
   #+end_src
3. Use stow
#+begin_src sh
cd ~/.dotfiles/
stow .
#+end_src
If everything worked right, you should not get any errors. If you have duplicates, it will inform you. In case of duplicates, remove them from your home directory and rerun ~stow .~
4. (Optional) Push to Git
